<?php

class Animal{

    //properties
    protected $legs = 2;
    protected $cold_blooded = "false";
    protected $name;

    //method

    //constructor
    function __construct($jeneng){
        $this->name = $jeneng;
    }

    //get
    function get_name(){
        return $this->name;
    }
    function get_legs(){
        return $this->legs;
    }
    function get_cold_blooded(){
        return $this->cold_blooded;
    }

    //set
    function set_legs($n){
        $this->legs = $n;
    }

}

class Ape extends Animal{
    function yell(){
        echo "Auooo";
    }
}

class Frog extends Animal{
    function jump(){
        echo "Hop hop";
    }
}

?>
