<?php

//release 0
require_once 'includes/animal.php';

$sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"        //kalau public baru bisa diakses
// echo "<br>";
// echo $sheep->legs; // 2
// echo "<br>";
// echo $sheep->cold_blooded; // false
// echo "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<h5>Ini Pakai Get</h5>";

echo $sheep->get_name();
echo "<br>";
echo $sheep->get_legs();
echo "<br>";
echo $sheep->get_cold_blooded();
echo "<br>";

//release 1

echo "<h5>Ini Inheritance Animal</h5>";

$sungokong = new Ape("kera sakti");
echo $sungokong->get_name();
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
echo "<br>";
$kodok->set_legs(4);
echo $kodok->get_legs();

?>